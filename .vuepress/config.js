module.exports = {
  port: "3000",
  dest: "docs",
  ga: "UA-85414008-1",
  base: "/",
  markdown: {
    externalLinks: {
      target: '_blank', rel: 'noopener noreferrer'
    }
  },
  locales: {
    "/": {
      lang: "zh-CN",
      title: "CollectiveOAuth-授权库",
      description: ".Net(C#)-史上最全的整合第三方登录的开源库"
    },
  },
  head: [["link", { rel: "icon", href: `/favicon.ico` }]],
  themeConfig: {
    editLinks: true,
    locales: {
      "/": {
        label: "简体中文",
        selectText: "Languages",
        editLinkText: "在 Gitee 上编辑此页",
        lastUpdated: "上次更新",
        nav: [
          {
            text: "指南",
            link: "/guide/introduction.html"
          },
          {
            text: "快速入门",
            link: "/guide/basic-config.html"
          },
        ],
        sidebar: {
          "/guide/": genGuideSidebar(true),
        }
      },
    }
  }
};

function genGuideSidebar(isZh) {
  return [
    {
      title: "指南",
      collapsable: false,
      children: ["introduction"]
    },
    {
      title: "快速入门",
      collapsable: false,
      children: ["basic-config", "single-use", "factory-use"]
    },
  ]
}

