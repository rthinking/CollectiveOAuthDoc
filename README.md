---
home: true
heroImage: /img/logo.png
actionText: 快速开始 →
actionLink: /guide/introduction.html
features:
- title: CollectiveOAuth
  details: 它仅仅是一个 第三方授权登录的工具类库，它可以让我们脱离繁琐的第三方登录SDK，让登录变得So easy!
- title: 特点(全)
  details: 已集成十多家第三方平台（国内外常用的基本都已包含），仍然还在持续扩展中。
- title: 特点(简)
  details: API就是奔着最简单去设计的（见后面快速开始），尽量让您用起来没有障碍感。
footer: MIT | © 2016-2020 wei.fu
---


